package ala.models;

public interface LuciferAbilitiesModel {
    /**
      * manage Lucifer jumps on platforms.
      * 
      */
    void jumpManager();
}
