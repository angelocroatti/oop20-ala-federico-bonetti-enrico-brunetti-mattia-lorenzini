package ala.views;

public interface LuciferAbilitiesView {
    /**
     * Return true il Lucifer sprite is right and False if is left.
     * @return boolean
     */
    boolean luciferDirection();
}
